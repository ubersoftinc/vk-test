import Vue from 'vue'

import App from '../imports/ui/pages/App.vue'

import '../imports/ui/components'

Meteor.startup(() => {
  new Vue({
    el: '#app',
    ...App,
  })
})