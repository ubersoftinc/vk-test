import Vue from 'vue'

import CustomImage from './CustomImage.vue'
Vue.component('custom-image', {
  props: { 
    id: Number,
    url: String,
    starred: Boolean,
  },
  ...CustomImage
})

import CustomInput from './CustomInput.vue'
Vue.component('custom-input', CustomInput)
